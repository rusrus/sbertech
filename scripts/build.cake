#tool "nuget:https://www.nuget.org/api/v2?package=NUnit.ConsoleRunner";

var target = Argument("target", "Default");
var nugetConfigFile = Argument("nugetConfigFile","./Nuget.config");
var solutionPath = "./../src/Sbertech.sln";
var configuration = "Debug";
var artifactsPath = "./../artifacts";
var appArtifactsPath = "./../artifacts/app";

Task("Build")
    .Does(() =>
    {
        var nuGetSettings = new NuGetRestoreSettings 
		{ 
			NoCache = true,
			ConfigFile = nugetConfigFile
		};

		NuGetRestore(solutionPath, nuGetSettings);
		
        var buildSettings = new MSBuildSettings
        {
            Verbosity = Verbosity.Minimal,
            ToolVersion = MSBuildToolVersion.VS2017,
            Configuration = configuration,
        };

        MSBuild(solutionPath, buildSettings);
    });

Task("Test:Unit")
    .IsDependentOn("Build")
    .Does(() => 
    {
        var settings = new NUnit3Settings
        {
            Work = "./../artifacts/test-results"
        };
        var unitTests = GetFiles("./../src/Tests/**/bin/" + configuration + "/*.Tests.dll");
        NUnit3(unitTests, settings);
    });

Task("Publish")
    .IsDependentOn("Build")
    .Does(() => 
    {
        if(DirectoryExists(appArtifactsPath))
        {
            DeleteDirectory(appArtifactsPath, new DeleteDirectorySettings {Recursive = true, Force = true});
        }
        CreateDirectory(appArtifactsPath);
        CopyFiles("./../src/Sbertech.Chess.Ui/bin/" + configuration + "/*.dll", appArtifactsPath);
        CopyFiles("./../src/Sbertech.Chess.Ui/bin/" + configuration + "/*.exe", appArtifactsPath);
        CopyFiles("./../src/Sbertech.Chess.Ui/bin/" + configuration + "/*.exe.config", appArtifactsPath);
    });

Task("Default")
    .IsDependentOn("Build")
    .IsDependentOn("Test:Unit")
    .IsDependentOn("Publish");

RunTarget(target);