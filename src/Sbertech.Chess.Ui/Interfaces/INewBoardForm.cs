﻿using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Ui.Interfaces
{
    public interface INewBoardForm
    {
        Size? AskBorderSize(int width, int height);
    }
}
