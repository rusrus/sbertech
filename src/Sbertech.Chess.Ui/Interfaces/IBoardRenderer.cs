﻿using System.Drawing;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Ui.Interfaces
{
    public interface IBoardRenderer
    {
        void Render(IGraphics graphics, Board board, Color backColor);
    }
}