﻿using System;
using System.Drawing;

namespace Sbertech.Chess.Ui.Interfaces
{
    public interface IGraphics : IDisposable
    {
        int Height { get; }

        int Width { get; }

        void Clear(Color backColor);

        void DrawLine(Pen pen, int fromX, int fromY, int toX, int toY);

        void FillRectangle(Brush brush, Rectangle rectangle);

        void FillEllipse(Brush brush, int x, int y, int width, int height);

        void FillPolygon(Brush brush, Point[] points);
    }
}
