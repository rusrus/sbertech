﻿using System.Drawing;
using System.Windows.Forms;

namespace Sbertech.Chess.Ui.Interfaces
{
    public interface IGraphicsFactory
    {
        IGraphics Create(Control control);

        IGraphics Create(Bitmap bitmap);
    }
}
