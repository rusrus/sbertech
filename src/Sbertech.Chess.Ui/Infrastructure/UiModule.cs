﻿using Autofac;
using Sbertech.Chess.Ui.Forms;
using Sbertech.Chess.Ui.Interfaces;
using Sbertech.Chess.Ui.Renderers;

namespace Sbertech.Chess.Ui.Infrastructure
{
    public class UiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BoardRenderer>().As<IBoardRenderer>();

            builder.RegisterType<MainForm>().SingleInstance().AsSelf();
            builder.RegisterType<NewBoardForm>().As<INewBoardForm>();
            builder.RegisterType<GraphicsFactory>().As<IGraphicsFactory>().SingleInstance();

            base.Load(builder);
        }
    }
}