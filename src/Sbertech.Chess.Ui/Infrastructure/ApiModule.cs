﻿using Autofac;
using Sbertech.Chess.Api.Factories;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Players;

namespace Sbertech.Chess.Ui.Infrastructure
{
    public class ApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ShapeFactory>().As<IShapeFactory>().SingleInstance();
            builder.RegisterType<Player>().As<IPlayer>();
            builder.RegisterType<PlayerCommandsFactory>().As<IPlayerCommandsFactory>().SingleInstance();
            base.Load(builder);
        }
    }
}
