﻿using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Ui.Extensions
{
    public static class BoardExtensions
    {
        public static int GetCellSize(this Board board, int width, int height)
        {
            var cellSizeWidth = width / board.Width;
            var cellSizeHeight = height / board.Height;
            return cellSizeWidth < cellSizeHeight ? cellSizeWidth : cellSizeHeight;
        }
    }
}