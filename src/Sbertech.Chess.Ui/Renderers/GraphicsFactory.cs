﻿using System.Drawing;
using System.Windows.Forms;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public class GraphicsFactory : IGraphicsFactory
    {
        public IGraphics Create(Control control)
        {
            return new GraphicsAdapter(control.CreateGraphics());
        }

        public IGraphics Create(Bitmap bitmap)
        {
            return new GraphicsAdapter(Graphics.FromImage(bitmap));
        }
    }
}
