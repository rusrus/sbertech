﻿using System.Drawing;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public class GraphicsAdapter : IGraphics
    {
        private readonly Graphics _graphics;

        public GraphicsAdapter(Graphics graphics)
        {
            _graphics = graphics;
        }

        public int Width => (int)_graphics.VisibleClipBounds.Width;

        public int Height => (int)_graphics.VisibleClipBounds.Height;

        public void Dispose()
        {
            _graphics?.Dispose();
        }

        public void Clear(Color backColor)
        {
            _graphics.Clear(backColor);
        }

        public void DrawLine(Pen pen, int fromX, int fromY, int toX, int toY)
        {
            _graphics.DrawLine(pen, fromX, fromY, toX, toY);
        }

        public void FillRectangle(Brush brush, Rectangle rectangle)
        {
            _graphics.FillRectangle(brush, rectangle);
        }

        public void FillEllipse(Brush brush, int x, int y, int width, int height)
        {
            _graphics.FillEllipse(brush, x, y, width, height);
        }

        public void FillPolygon(Brush brush, Point[] points)
        {
            _graphics.FillPolygon(brush, points);
        }
    }
}
