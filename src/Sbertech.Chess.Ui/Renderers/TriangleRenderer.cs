﻿using System.Drawing;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public class TriangleRenderer : ShapeRenderer
    {
        protected override void Render(IGraphics graphics, int x, int y, int shapeSize)
        {
            var halfShapeSize = shapeSize / 2;

            var pointsOfTriangle = new Point[]
            {
                new Point(x - halfShapeSize, y + halfShapeSize),
                new Point(x, y - halfShapeSize),
                new Point(x + halfShapeSize, y + halfShapeSize),
            };

            graphics.FillPolygon(Brushes.Blue, pointsOfTriangle);
        }
    }
}