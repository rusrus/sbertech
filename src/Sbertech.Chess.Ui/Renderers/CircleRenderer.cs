﻿using System.Drawing;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public class CircleRenderer : ShapeRenderer
    {
        protected override void Render(IGraphics graphics, int x, int y, int shapeSize)
        {
            var halfSize = shapeSize / 2;

            graphics.FillEllipse(Brushes.Chocolate, x - halfSize, y - halfSize, shapeSize, shapeSize);
        }
    }
}
