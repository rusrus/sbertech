﻿using System.Collections.Generic;
using System.Drawing;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Ui.Extensions;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public class BoardRenderer : IBoardRenderer
    {
        private static readonly Dictionary<ShapeType, ShapeRenderer> Renderers = new Dictionary<ShapeType, ShapeRenderer>
        {
            { ShapeType.Circle, new CircleRenderer() },
            { ShapeType.Triangle, new TriangleRenderer() },
            { ShapeType.Square, new SquareRenderer() },
        };

        public void Render(IGraphics graphics, Board board, Color backColor)
        {
            graphics.Clear(backColor);
            var cellSize = board.GetCellSize(graphics.Width, graphics.Height);

            var boardSizeWidth = board.Width * cellSize;
            var boardSizeHeight = board.Height * cellSize;

            var currentY = 0;

            for (var i = 0; i <= board.Height; i++)
            {
                graphics.DrawLine(Pens.Black, 0, currentY, boardSizeWidth, currentY);
                currentY += cellSize;
            }

            var currentX = 0;

            for (var i = 0; i <= board.Width; i++)
            {
                graphics.DrawLine(Pens.Black, currentX, 0, currentX, boardSizeHeight);
                currentX += cellSize;
            }

            var half = cellSize / 2;
            foreach (var shape in board.Shapes)
            {
                if (shape == board.ActiveShape)
                {
                    graphics.FillRectangle(Brushes.Yellow, new Rectangle(shape.Position.X * cellSize + 1, shape.Position.Y * cellSize + 1, cellSize - 1, cellSize - 1));
                }

                var shapeCenterX = shape.Position.X * cellSize + half;
                var shapeCenterY = shape.Position.Y * cellSize + half;

                foreach (var neighbor in board.GetNeighbors(shape.Position))
                {
                    var moving = shape.Position - neighbor.Position;
                    graphics.DrawLine(Pens.Red, shapeCenterX + moving.Dx * half, shapeCenterY + moving.Dy * half, shapeCenterX, shapeCenterY);
                }

                var renderer = Renderers[shape.ShapeType];
                renderer.Render(graphics, shape, cellSize);
            }
        }
    }
}
