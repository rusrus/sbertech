﻿using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Ui.Constants;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public abstract class ShapeRenderer
    {
        public void Render(IGraphics graphics, Shape shape, int cellSize)
        {
            var shapeSize = GetShapeSize(cellSize);
            var (x, y) = GetCoordinates(shape, cellSize);

            Render(graphics, x, y, shapeSize);
        }

        protected abstract void Render(IGraphics graphics, int x, int y, int shapeSize);

        private static (int, int) GetCoordinates(Shape shape, int cellSize) => (shape.Position.X * cellSize + cellSize / 2, shape.Position.Y * cellSize + cellSize / 2);

        private static int GetShapeSize(int cellSize) => (int)(cellSize * DisplayConstants.CellSize2ShapeSizeAspectRatio);
    }
}