﻿using System.Drawing;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Renderers
{
    public class SquareRenderer : ShapeRenderer
    {
        protected override void Render(IGraphics graphics, int x, int y, int shapeSize)
        {
            var halfShapeSize = shapeSize / 2;
            graphics.FillRectangle(Brushes.Green, new Rectangle(x - halfShapeSize, y - halfShapeSize, shapeSize, shapeSize));
        }
    }
}