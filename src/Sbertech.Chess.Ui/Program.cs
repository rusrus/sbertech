﻿using System;
using System.Windows.Forms;
using Autofac;
using Sbertech.Chess.Ui.Forms;
using Sbertech.Chess.Ui.Infrastructure;

namespace Sbertech.Chess.Ui
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var container = CreateContainer();

            using (var lifeTimeScope = container.BeginLifetimeScope())
            {
                var mainForm = lifeTimeScope.Resolve<MainForm>();

                Application.Run(mainForm);
            }
        }

        private static IContainer CreateContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule<ApiModule>();
            builder.RegisterModule<UiModule>();
            return builder.Build();
        }
    }
}
