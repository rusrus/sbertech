﻿namespace Sbertech.Chess.Ui.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.newBoardButton = new System.Windows.Forms.ToolStripButton();
            this.separator1 = new System.Windows.Forms.ToolStripSeparator();
            this.useCircleButton = new System.Windows.Forms.ToolStripButton();
            this.useTriangleButton = new System.Windows.Forms.ToolStripButton();
            this.useSquareButton = new System.Windows.Forms.ToolStripButton();
            this.separator2 = new System.Windows.Forms.ToolStripSeparator();
            this.nextStepButton = new System.Windows.Forms.ToolStripButton();
            this.boardBox = new System.Windows.Forms.PictureBox();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boardBox)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip
            // 
            this.toolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBoardButton,
            this.separator1,
            this.useCircleButton,
            this.useTriangleButton,
            this.useSquareButton,
            this.separator2,
            this.nextStepButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 0);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip.Size = new System.Drawing.Size(1392, 25);
            this.toolStrip.TabIndex = 0;
            // 
            // newBoardButton
            // 
            this.newBoardButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.newBoardButton.Image = ((System.Drawing.Image)(resources.GetObject("newBoardButton.Image")));
            this.newBoardButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newBoardButton.Name = "newBoardButton";
            this.newBoardButton.Size = new System.Drawing.Size(79, 22);
            this.newBoardButton.Text = "Новая доска";
            this.newBoardButton.Click += new System.EventHandler(this.NewBoard_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            this.separator1.Size = new System.Drawing.Size(6, 25);
            // 
            // useCircleButton
            // 
            this.useCircleButton.Checked = true;
            this.useCircleButton.CheckOnClick = true;
            this.useCircleButton.CheckState = System.Windows.Forms.CheckState.Checked;
            this.useCircleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.useCircleButton.Image = ((System.Drawing.Image)(resources.GetObject("useCircleButton.Image")));
            this.useCircleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.useCircleButton.Name = "useCircleButton";
            this.useCircleButton.Size = new System.Drawing.Size(36, 22);
            this.useCircleButton.Text = "Круг";
            this.useCircleButton.Click += new System.EventHandler(this.AddCircle_Click);
            // 
            // useTriangleButton
            // 
            this.useTriangleButton.CheckOnClick = true;
            this.useTriangleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.useTriangleButton.Image = ((System.Drawing.Image)(resources.GetObject("useTriangleButton.Image")));
            this.useTriangleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.useTriangleButton.Name = "useTriangleButton";
            this.useTriangleButton.Size = new System.Drawing.Size(82, 22);
            this.useTriangleButton.Text = "Треугольник";
            this.useTriangleButton.Click += new System.EventHandler(this.AddTriangle_Click);
            // 
            // useSquareButton
            // 
            this.useSquareButton.CheckOnClick = true;
            this.useSquareButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.useSquareButton.Image = ((System.Drawing.Image)(resources.GetObject("useSquareButton.Image")));
            this.useSquareButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.useSquareButton.Name = "useSquareButton";
            this.useSquareButton.Size = new System.Drawing.Size(54, 22);
            this.useSquareButton.Text = "Квадрат";
            this.useSquareButton.Click += new System.EventHandler(this.AddSquare_Click);
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Size = new System.Drawing.Size(6, 25);
            // 
            // nextStepButton
            // 
            this.nextStepButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.nextStepButton.Image = ((System.Drawing.Image)(resources.GetObject("nextStepButton.Image")));
            this.nextStepButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.nextStepButton.Name = "nextStepButton";
            this.nextStepButton.Size = new System.Drawing.Size(94, 22);
            this.nextStepButton.Text = "Следущий шаг";
            this.nextStepButton.ToolTipText = "Следущий шаг";
            this.nextStepButton.Click += new System.EventHandler(this.NexStep_Click);
            // 
            // boardBox
            // 
            this.boardBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.boardBox.InitialImage = null;
            this.boardBox.Location = new System.Drawing.Point(0, 25);
            this.boardBox.Name = "boardBox";
            this.boardBox.Size = new System.Drawing.Size(1392, 708);
            this.boardBox.TabIndex = 1;
            this.boardBox.TabStop = false;
            this.boardBox.SizeChanged += new System.EventHandler(this.BoardBox_SizeChanged);
            this.boardBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.BoardPanel_MouseClick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1392, 733);
            this.Controls.Add(this.boardBox);
            this.Controls.Add(this.toolStrip);
            this.Name = "MainForm";
            this.Text = "Admission task";
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.boardBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripButton useCircleButton;
        private System.Windows.Forms.ToolStripButton useTriangleButton;
        private System.Windows.Forms.ToolStripButton useSquareButton;
        private System.Windows.Forms.ToolStripSeparator separator2;
        private System.Windows.Forms.ToolStripButton nextStepButton;
        private System.Windows.Forms.ToolStripButton newBoardButton;
        private System.Windows.Forms.ToolStripSeparator separator1;
        private System.Windows.Forms.PictureBox boardBox;
    }
}

