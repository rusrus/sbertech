﻿using System;
using System.Windows.Forms;
using Sbertech.Chess.Api.Constants;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Forms
{
    public partial class NewBoardForm : Form, INewBoardForm
    {
        private Size _borderSize;

        public NewBoardForm()
        {
            InitializeComponent();
            tbWidth.Maximum = BoardContants.MaxHeight;
            tbHeight.Maximum = BoardContants.MaxWidth;
        }

        public Size? AskBorderSize(int width, int height)
        {
            tbWidth.Value = width;
            tbHeight.Value = height;

            return ShowDialog() == DialogResult.OK ? _borderSize : (Size?)null;
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            _borderSize = new Size((int)tbWidth.Value, (int)tbHeight.Value);
            Close();
        }
    }
}
