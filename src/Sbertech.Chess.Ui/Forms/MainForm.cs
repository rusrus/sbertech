﻿using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Ui.Extensions;
using Sbertech.Chess.Ui.Interfaces;

namespace Sbertech.Chess.Ui.Forms
{
    public partial class MainForm : Form
    {
        private readonly IBoardRenderer _boardRenderer;
        private readonly Func<INewBoardForm> _newBoardFormFunc;
        private readonly Board _board;
        private readonly IPlayer _player;
        private readonly IGraphicsFactory _graphicsFactory;
        private ShapeType? _currentShapeType = ShapeType.Circle;

        public MainForm(IBoardRenderer boardRenderer, IShapeFactory shapeFactory, Func<INewBoardForm> newBoardFormFunc, IPlayer player, IGraphicsFactory graphicsFactory)
        {
            _newBoardFormFunc = newBoardFormFunc;
            _boardRenderer = boardRenderer;

            _board = new Board(shapeFactory);
            _player = player;
            _graphicsFactory = graphicsFactory;
            _player.Initialize(_board);

            InitializeComponent();
            RedrawBoard();
        }

        private void NewBoard_Click(object sender, EventArgs e)
        {
            var dialogForm = _newBoardFormFunc();
            var size = dialogForm.AskBorderSize(_board.Width, _board.Height);
            if (size == null)
            {
                return;
            }

            _board.SetSize(size.Value);
            RedrawBoard();
        }

        private void AddCircle_Click(object sender, EventArgs e)
        {
            ChangeShateType(ShapeType.Circle, (ToolStripButton)sender);
        }

        private void AddTriangle_Click(object sender, EventArgs e)
        {
            ChangeShateType(ShapeType.Triangle, (ToolStripButton)sender);
        }

        private void AddSquare_Click(object sender, EventArgs e)
        {
            ChangeShateType(ShapeType.Square, (ToolStripButton)sender);
        }

        private void ChangeShateType(ShapeType typeShapeForAdd, ToolStripButton sender)
        {
            if (!sender.Checked)
            {
                _currentShapeType = null;
            }
            else
            {
                _currentShapeType = typeShapeForAdd;
                useCircleButton.Checked = useCircleButton == sender;
                useSquareButton.Checked = useSquareButton == sender;
                useTriangleButton.Checked = useTriangleButton == sender;
            }
        }

        private void BoardPanel_MouseClick(object sender, MouseEventArgs e)
        {
            if (_currentShapeType == null)
            {
                return;
            }

            var cellSize = _board.GetCellSize(boardBox.Width, boardBox.Height);

            var x = (int)Math.Floor((decimal)(e.X / cellSize));
            var y = (int)Math.Floor((decimal)(e.Y / cellSize));

            var position = new Position(x, y);

            switch (e.Button)
            {
                case MouseButtons.Left:
                    if (!_board.TryAdd(_currentShapeType.Value, position, out var shape))
                    {
                        return;
                    }

                    break;

                case MouseButtons.Right:
                    var remove = _board.Get(position);

                    if (remove == null)
                    {
                        return;
                    }

                    _board.Remove(remove);
                    break;

                default:
                    return;
            }

            RedrawBoard();
        }

        private async void NexStep_Click(object sender, EventArgs e)
        {
            var button = (ToolStripButton)sender;
            button.Enabled = false;
            await Task.Yield();

            while (!_player.Play())
            {
                RedrawBoard();
                await Task.Delay(10).ConfigureAwait(true);
            }

            RedrawBoard();
            button.Enabled = true;
        }

        private void RedrawBoard()
        {
            var bitmap = new Bitmap(boardBox.Width, boardBox.Height);
            using (var graphics = _graphicsFactory.Create(bitmap))
            {
                _boardRenderer.Render(graphics, _board, BackColor);
            }

            boardBox.Image = bitmap;
        }

        private void BoardBox_SizeChanged(object sender, EventArgs e)
        {
            RedrawBoard();
        }
    }
}
