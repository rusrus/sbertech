﻿namespace Sbertech.Chess.Ui.Forms
{
    partial class NewBoardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lWidth = new System.Windows.Forms.Label();
            this.lHeight = new System.Windows.Forms.Label();
            this.bOk = new System.Windows.Forms.Button();
            this.tbWidth = new System.Windows.Forms.NumericUpDown();
            this.tbHeight = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.tbWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHeight)).BeginInit();
            this.SuspendLayout();
            // 
            // lWidth
            // 
            this.lWidth.AutoSize = true;
            this.lWidth.Location = new System.Drawing.Point(12, 26);
            this.lWidth.Name = "lWidth";
            this.lWidth.Size = new System.Drawing.Size(46, 13);
            this.lWidth.TabIndex = 0;
            this.lWidth.Text = "Ширина";
            // 
            // lHeight
            // 
            this.lHeight.AutoSize = true;
            this.lHeight.Location = new System.Drawing.Point(131, 26);
            this.lHeight.Name = "lHeight";
            this.lHeight.Size = new System.Drawing.Size(45, 13);
            this.lHeight.TabIndex = 1;
            this.lHeight.Text = "Высота";
            // 
            // bOk
            // 
            this.bOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bOk.Location = new System.Drawing.Point(146, 66);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(76, 25);
            this.bOk.TabIndex = 4;
            this.bOk.Text = "ОК";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.Ok_Click);
            // 
            // tbWidth
            // 
            this.tbWidth.Location = new System.Drawing.Point(65, 22);
            this.tbWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbWidth.Name = "tbWidth";
            this.tbWidth.Size = new System.Drawing.Size(46, 20);
            this.tbWidth.TabIndex = 5;
            this.tbWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbHeight
            // 
            this.tbHeight.Location = new System.Drawing.Point(180, 22);
            this.tbHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbHeight.Name = "tbHeight";
            this.tbHeight.Size = new System.Drawing.Size(46, 20);
            this.tbHeight.TabIndex = 6;
            this.tbHeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // NewBoardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(236, 110);
            this.Controls.Add(this.tbHeight);
            this.Controls.Add(this.tbWidth);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.lHeight);
            this.Controls.Add(this.lWidth);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NewBoardForm";
            this.Text = "Новая доска";
            ((System.ComponentModel.ISupportInitialize)(this.tbWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbHeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lWidth;
        private System.Windows.Forms.Label lHeight;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.NumericUpDown tbWidth;
        private System.Windows.Forms.NumericUpDown tbHeight;
    }
}