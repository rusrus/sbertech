﻿using System;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using Sbertech.Chess.Api.Exceptions;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Tests
{
    public class BoardTests
    {
        private Random _random;

        [SetUp]
        public void SetUp()
        {
            _random = new Random(Guid.NewGuid().GetHashCode());
        }

        [TestCase(-1, 1)]
        [TestCase(-1, -1)]
        [TestCase(1, -1)]
        [TestCase(0, 1)]
        [TestCase(0, 0)]
        [TestCase(1, 0)]
        public void SetSize_InvalidBoardSize_InvalidBoardExceptionThrown(int x, int y)
        {
            var factory = Substitute.For<IShapeFactory>();
            var board = new Board(factory);
            Assert.Throws<InvalidBoardException>(() => board.SetSize(new Size(x, y)));
        }

        [TestCase(-1, 1)]
        [TestCase(1, -1)]
        [TestCase(-1, -1)]
        [TestCase(0, 3)]
        [TestCase(3, 0)]
        [TestCase(3, 3)]
        public void Add_InvalidShapeLocation_InvalidShapeExceptionThrown(int x, int y)
        {
            var board = CrateBoard(3, 3);
            Assert.Throws<InvalidShapeException>(() => board.Add(ShapeType.Circle, new Position(x, y)));
        }

        [TestCase(-1, 1, ExpectedResult = false)]
        [TestCase(1, -1, ExpectedResult = false)]
        [TestCase(-1, -1, ExpectedResult = false)]
        [TestCase(0, 3, ExpectedResult = false)]
        [TestCase(3, 0, ExpectedResult = false)]
        [TestCase(3, 3, ExpectedResult = false)]
        [TestCase(0, 0, ExpectedResult = true)]
        public bool TryAdd_Success(int x, int y)
        {
            var board = CrateBoard(3, 3);
            return board.TryAdd(ShapeType.Circle, new Position(x, y), out var shape);
        }

        [Test]
        public void Add_ValidShape_Success()
        {
            var board = CrateBoard(3, 3);

            var position = CreateRandomPosition(3, 3);

            var result = board.Add(ShapeType.Circle, position);
            Assert.NotNull(result);
            Assert.That(board.Shapes.Count, Is.EqualTo(1));
            Assert.That(board.Shapes.First(), Is.EqualTo(result));
            Assert.That(result.Position.X, Is.EqualTo(position.X));
            Assert.That(result.Position.Y, Is.EqualTo(position.Y));
        }

        [Test]
        public void Get_NotExists_Null()
        {
            var position = CreateRandomPosition(3, 3);

            var board = CrateBoard(3, 3);
            var actual = board.Get(position);

            Assert.Null(actual);
        }

        [Test]
        public void Get_Exists_Success()
        {
            var position = CreateRandomPosition(3, 3);

            var board = CrateBoard(3, 3);
            board.Add(ShapeType.Circle, position);

            var actual = board.Get(position);

            Assert.That(actual.Position, Is.EqualTo(position));
        }

        [Test]
        public void GetNeighbors_Success()
        {
            var board = CrateBoard(3, 3);

            board.Add(ShapeType.Circle, new Position(0, 0));
            var excepted1 = board.Add(ShapeType.Circle, new Position(0, 1));
            board.Add(ShapeType.Circle, new Position(0, 2));
            var excepted2 = board.Add(ShapeType.Circle, new Position(1, 0));
            board.Add(ShapeType.Circle, new Position(1, 1));
            board.Add(ShapeType.Circle, new Position(1, 2));
            board.Add(ShapeType.Circle, new Position(2, 0));
            board.Add(ShapeType.Circle, new Position(2, 1));
            board.Add(ShapeType.Circle, new Position(2, 2));

            var position = new Position(0, 0);
            var actual = board.GetNeighbors(position);

            Assert.That(actual, Is.EquivalentTo(new[] { excepted1, excepted2 }));
        }

        [Test]
        public void GetNeighbors_NoNeighors_Empty()
        {
            var board = CrateBoard(3, 3);

            var position = CreateRandomPosition(3, 3);
            var actual = board.GetNeighbors(position);

            Assert.IsEmpty(actual);
        }

        [Test]
        public void Add_ShapeInExistingLocation_InvalidShapeExceptionThrown()
        {
            var board = CrateBoard(3, 3);

            board.Add(ShapeType.Circle, new Position(1, 1));
            Assert.Throws<InvalidShapeException>(() => board.Add(ShapeType.Circle, new Position(1, 1)));
        }

        [Test]
        public void Move_Success()
        {
            var board = CrateBoard(3, 3);

            var shape = board.Add(ShapeType.Circle, new Position(1, 1));
            var moving = new Moving(1, 0);
            board.Move(shape, moving);

            Assert.That(shape.Position, Is.EqualTo(new Position(2, 1)));
        }

        [TestCase(1, 0)]
        [TestCase(1, 1)]
        [TestCase(0, 1)]
        public void Move_Out_MovingExceptionThrown(int dx, int dy)
        {
            var board = CrateBoard(3, 4);

            var shape = board.Add(ShapeType.Circle, new Position(2, 3));
            var moving = new Moving(dx, dy);

            Assert.Throws<MoveOutOfBoardException>(() => board.Move(shape, moving));
        }

        private Board CrateBoard(int width, int height)
        {
            var factory = CreateFactory();
            var board = new Board(factory);
            board.SetSize(new Size(width, height));
            return board;
        }

        private IShapeFactory CreateFactory()
        {
            var factory = Substitute.For<IShapeFactory>();
            factory.Create(Arg.Is(ShapeType.Circle)).Returns(info => new Triangle());
            factory.Create(Arg.Is(ShapeType.Triangle)).Returns(info => new Triangle());
            factory.Create(Arg.Is(ShapeType.Square)).Returns(info => new Triangle());

            return factory;
        }

        private Position CreateRandomPosition(int xmax, int ymax)
        {
            var x = _random.Next(xmax);
            var y = _random.Next(ymax);
            return new Position(x, y);
        }
    }
}
