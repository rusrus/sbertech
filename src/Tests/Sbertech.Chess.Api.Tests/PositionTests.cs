﻿using NUnit.Framework;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Tests
{
    public class PositionTests
    {
        [TestCase(4, 6, ExpectedResult = true)]
        [TestCase(4, 0, ExpectedResult = false)]
        [TestCase(0, 6, ExpectedResult = false)]
        [TestCase(0, 0, ExpectedResult = false)]
        public bool EqualOperator_Success(int x, int y)
        {
            var position = new Position(4, 6);
            var testPosition = new Position(x, y);

            return position == testPosition;
        }

        [TestCase(4, 6, ExpectedResult = false)]
        [TestCase(4, 0, ExpectedResult = true)]
        [TestCase(0, 6, ExpectedResult = true)]
        [TestCase(0, 0, ExpectedResult = true)]
        public bool NotEqualOperator_Success(int x, int y)
        {
            var position = new Position(4, 6);
            var testPosition = new Position(x, y);

            return position != testPosition;
        }

        [Test]
        public void PlusOperator_Success()
        {
            var position = new Position(4, 6);
            var moving = new Moving(3, 5);

            var actual = position + moving;
            Assert.That(actual, Is.EqualTo(new Position(7, 11)));
        }
    }
}
