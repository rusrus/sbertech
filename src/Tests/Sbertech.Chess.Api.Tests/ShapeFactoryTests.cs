﻿using System;
using NUnit.Framework;
using Sbertech.Chess.Api.Factories;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Tests
{
    public class ShapeFactoryTests
    {
        [TestCase(ShapeType.Circle, typeof(Circle))]
        [TestCase(ShapeType.Triangle, typeof(Triangle))]
        [TestCase(ShapeType.Square, typeof(Square))]
        public void Create_Shape_Success(ShapeType type, Type expectedType)
        {
            var factory = new ShapeFactory();
            var result = factory.Create(type);

            Assert.That(result, Is.TypeOf(expectedType));
        }
    }
}
