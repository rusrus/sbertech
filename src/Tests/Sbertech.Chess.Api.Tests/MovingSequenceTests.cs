﻿using NUnit.Framework;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Tests
{
    public class MovingSequenceTests
    {
        [Test]
        public void Distance_SumOfDistances()
        {
            var movings = new MovingSequence(new[] { Moving.Right(), Moving.DownRight(), Moving.Down(), });
            Assert.That(movings.Distance, Is.EqualTo(4));
        }

        [Test]
        public void Distance_Empty_0()
        {
            var movings = new MovingSequence(new Moving[] { });
            Assert.That(movings.Distance, Is.EqualTo(0));
        }

        [Test]
        public void TargetMoving_SumOfDistances()
        {
            var movings = new MovingSequence(new[] { Moving.Right(), Moving.DownRight(), Moving.Down(), });
            Assert.That(movings.TargetMoving, Is.EqualTo(new Moving(2, 2)));
        }
    }
}
