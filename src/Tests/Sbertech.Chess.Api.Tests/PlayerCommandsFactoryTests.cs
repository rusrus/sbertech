﻿using NUnit.Framework;
using Sbertech.Chess.Api.Factories;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Api.Players;
using Sbertech.Chess.Testing.Common;

namespace Sbertech.Chess.Api.Tests
{
    public class PlayerCommandsFactoryTests
    {
        [Test]
        public void Create_Success()
        {
            var factory = new PlayerCommandsFactory();
            var boardFactory = new BoardFactory();
            var board = boardFactory.Create(@"o..
                                              ..#
                                              ^^^
                                              ...");

            var actual = factory.CreateCommands(board);

            Assert.That(actual.Count, Is.EqualTo(10));
            Assert.That(actual[0].Shape, Is.EqualTo(board.Get(new Position(0, 0))));
            Assert.That(actual[0], Is.TypeOf<MovingCommand>());
            Assert.That(actual[1].Shape, Is.EqualTo(board.Get(new Position(0, 0))));
            Assert.That(actual[1], Is.TypeOf<SurviveCommand>());

            Assert.That(actual[2].Shape, Is.EqualTo(board.Get(new Position(0, 2))));
            Assert.That(actual[2], Is.TypeOf<MovingCommand>());
            Assert.That(actual[3].Shape, Is.EqualTo(board.Get(new Position(0, 2))));
            Assert.That(actual[3], Is.TypeOf<SurviveCommand>());

            Assert.That(actual[4].Shape, Is.EqualTo(board.Get(new Position(1, 2))));
            Assert.That(actual[4], Is.TypeOf<MovingCommand>());
            Assert.That(actual[5].Shape, Is.EqualTo(board.Get(new Position(1, 2))));
            Assert.That(actual[5], Is.TypeOf<SurviveCommand>());

            Assert.That(actual[6].Shape, Is.EqualTo(board.Get(new Position(2, 1))));
            Assert.That(actual[6], Is.TypeOf<MovingCommand>());
            Assert.That(actual[7].Shape, Is.EqualTo(board.Get(new Position(2, 1))));
            Assert.That(actual[7], Is.TypeOf<SurviveCommand>());

            Assert.That(actual[8].Shape, Is.EqualTo(board.Get(new Position(2, 2))));
            Assert.That(actual[8], Is.TypeOf<MovingCommand>());
            Assert.That(actual[9].Shape, Is.EqualTo(board.Get(new Position(2, 2))));
            Assert.That(actual[9], Is.TypeOf<SurviveCommand>());
        }
    }
}
