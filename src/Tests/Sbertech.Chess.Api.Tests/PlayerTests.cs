﻿using System.Linq;
using NSubstitute;
using NUnit.Framework;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Api.Players;
using Sbertech.Chess.Testing.Common;

namespace Sbertech.Chess.Api.Tests
{
    public class PlayerTests
    {
        private BoardFactory _boardFactory;
        private IPlayerCommandsFactory _commandsFactory;

        [SetUp]
        public void SetUp()
        {
            _boardFactory = new BoardFactory();
            _commandsFactory = Substitute.For<IPlayerCommandsFactory>();
        }

        [Test]
        public void Play_SingleCommand_True()
        {
            var board = _boardFactory.Create(@".o.
                                               o..
                                               ...");
            var shape = board.Get(new Position(0, 1));

            _commandsFactory.CreateCommands(Arg.Is(board)).Returns(new[] { new MovingCommand(board, shape), });

            var player = new Player(_commandsFactory);
            player.Initialize(board);

            var actual = player.Play();

            Assert.IsTrue(actual);
            Assert.That(shape.Position, Is.EqualTo(new Position(1, 1)));
        }

        [Test]
        public void Play_OneOfMultiplyCommands_False()
        {
            var board = _boardFactory.Create(@".o.
                                               o..
                                               ...");

            var commandsFactory = Substitute.For<IPlayerCommandsFactory>();
            commandsFactory.CreateCommands(Arg.Is(board)).Returns(board.Shapes.Select(shape => new MovingCommand(board, shape)).ToList());

            var player = new Player(commandsFactory);
            player.Initialize(board);

            var actual = player.Play();

            Assert.IsFalse(actual);
        }

        [Test]
        public void Play_SingleLongCommand_False()
        {
            var board = _boardFactory.Create(@"#..
                                               .#.
                                               ..#
                                               .#.");
            var shape = board.Get(new Position(0, 0));

            _commandsFactory.CreateCommands(Arg.Is(board)).Returns(new[] { new MovingCommand(board, shape), });

            var player = new Player(_commandsFactory);
            player.Initialize(board);

            var actual = player.Play();

            Assert.IsFalse(actual);
            Assert.That(shape.Position, Is.EqualTo(new Position(0, 1)));
        }

        [Test]
        public void Play_SurviveCommand_True()
        {
            var board = _boardFactory.Create(@".o.
                                               o..
                                               ...");
            var shape = board.Get(new Position(0, 1));

            _commandsFactory.CreateCommands(Arg.Is(board)).Returns(new[] { new SurviveCommand(board, shape), });

            var player = new Player(_commandsFactory);
            player.Initialize(board);

            var actual = player.Play();

            Assert.IsTrue(actual);
            Assert.That(board.Get(shape.Position), Is.Null);
        }

        [Test]
        public void Play_DontMove_True()
        {
            var board = _boardFactory.Create(@"...
                                               oo.
                                               ...");
            var shape = board.Get(new Position(0, 1));

            _commandsFactory.CreateCommands(Arg.Is(board)).Returns(new[] { new MovingCommand(board, shape), });

            var player = new Player(_commandsFactory);
            player.Initialize(board);

            var actual = player.Play();

            Assert.IsTrue(actual);
            Assert.That(shape.Position, Is.EqualTo(new Position(0, 1)));
        }
    }
}
