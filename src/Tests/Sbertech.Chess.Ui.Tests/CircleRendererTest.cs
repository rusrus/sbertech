﻿using System.Drawing;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using Sbertech.Chess.Testing.Common;
using Sbertech.Chess.Ui.Constants;
using Sbertech.Chess.Ui.Interfaces;
using Sbertech.Chess.Ui.Renderers;

namespace Sbertech.Chess.Ui.Tests
{
    public class CircleRendererTest
    {
        [Test]
        public void Render_Success()
        {
            var boardFactory = new BoardFactory();

            var board = boardFactory.Create(@"..
                                              ..
                                              .o");
            var shape = board.Shapes.First();
            var cellSize = 10;

            var graphics = Substitute.For<IGraphics>();

            var renderer = new CircleRenderer();
            renderer.Render(graphics, shape, cellSize);

            var diameter = (int)(cellSize * DisplayConstants.CellSize2ShapeSizeAspectRatio);
            graphics.Received().FillEllipse(Arg.Any<Brush>(), 12, 22, diameter, diameter);
        }
    }
}
