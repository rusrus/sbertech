﻿namespace Sbertech.Chess.Api.Constants
{
    public static class BoardContants
    {
        public const int DefaultWidth = 25;
        public const int DefaultHeight = 25;
        public const int MaxWidth = 100;
        public const int MaxHeight = 100;
    }
}