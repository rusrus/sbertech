﻿using System;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Exceptions
{
    public class MoveOutOfBoardException : Exception
    {
        public MoveOutOfBoardException(Board board, Shape shape, Moving moving)
            : base($"Can't move {shape.ShapeType} from {shape.Position} to {shape.Position + moving}. Out of board ({board.Width},{board.Height})")
        {
        }
    }
}
