﻿using System.Linq;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Players
{
    public class MovingCommand : PlayerCommand
    {
        private MovingSequence _moving;
        private int _step = 0;

        public MovingCommand(Board board, Shape shape)
            : base(board, shape)
        {
        }

        protected override void OnPlay()
        {
            if (_moving == null)
            {
                _moving = SelectMovings();
            }

            if (_moving == null)
            {
                CommandState = CommandState.Complete;
                return;
            }

            Board.Move(Shape, _moving.Steps[_step]);
            _step++;

            CommandState = _step == _moving.Steps.Count ? CommandState.Complete : CommandState.InProgress;
        }

        private MovingSequence SelectMovings()
        {
            var simulationResult = Shape.GetAllowedMovings()
                .Select(movings => Simulate(Shape, Board, movings))
                .Where(result => result.Reachable && result.LinksCount >= Shape.NeighborsToSurvive)
                .OrderByDescending(result => result.Movings.Distance)
                .FirstOrDefault();

            return simulationResult?.Movings;
        }

        private SimulationResult Simulate(Shape shape, Board board, MovingSequence movings)
        {
            var position = shape.Position;
            foreach (var moving in movings.Steps)
            {
                position += moving;
                if (board.Get(position) != null || position.X < 0 || position.X >= board.Width || position.Y < 0 || position.Y >= board.Height)
                {
                    return new SimulationResult
                    {
                        Movings = movings,
                        Reachable = false,
                    };
                }
            }

            return new SimulationResult
            {
                Movings = movings,
                Reachable = true,
                LinksCount = board.GetNeighbors(shape.Position + movings.TargetMoving).Count(n => n != shape),
            };
        }

        private class SimulationResult
        {
            public MovingSequence Movings { get; set; }

            public bool Reachable { get; set; }

            public int LinksCount { get; set; }
        }
    }
}