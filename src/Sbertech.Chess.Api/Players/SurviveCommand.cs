﻿using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Players
{
    public class SurviveCommand : PlayerCommand
    {
        public SurviveCommand(Board board, Shape shape)
            : base(board, shape)
        {
        }

        protected override void OnPlay()
        {
            CommandState = CommandState.Complete;

            var neighbors = Board.GetNeighbors(Shape.Position).Count;
            if (neighbors < Shape.NeighborsToSurvive)
            {
                Board.Remove(Shape);
            }
        }
    }
}