﻿using System.Collections.Generic;
using System.Linq;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Players
{
    public class Player : IPlayer
    {
        private readonly IPlayerCommandsFactory _commandsFactory;
        private Board _board;
        private IReadOnlyList<PlayerCommand> _commands;

        public Player(IPlayerCommandsFactory commandsFactory)
        {
            _commandsFactory = commandsFactory;
        }

        public void Initialize(Board board)
        {
            _board = board;
            _commands = null;
        }

        public bool Play()
        {
            var command = _commands?.FirstOrDefault(NotCompleted);
            if (command == null)
            {
                _commands = _commandsFactory.CreateCommands(_board);
                command = _commands?.FirstOrDefault(NotCompleted);
                if (command == null)
                {
                    return true;
                }
            }

            _board.ActiveShape = command.Shape;
            command.Play();

            return !_commands.Any(NotCompleted);
        }

        private bool NotCompleted(PlayerCommand command) => command.CommandState != CommandState.Complete;
    }
}
