﻿using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Players
{
    public abstract class PlayerCommand
    {
        protected PlayerCommand(Board board, Shape shape)
        {
            Board = board;
            Shape = shape;
            CommandState = CommandState.NotStarted;
        }

        public CommandState CommandState { get; protected set; }

        public Board Board { get; }

        public Shape Shape { get; }

        public virtual void Play()
        {
            CommandState = CommandState.InProgress;

            OnPlay();
        }

        protected abstract void OnPlay();
    }
}