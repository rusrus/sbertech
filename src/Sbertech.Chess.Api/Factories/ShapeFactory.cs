﻿using System;
using System.Collections.Generic;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Factories
{
    public class ShapeFactory : IShapeFactory
    {
        private static readonly Dictionary<ShapeType, Type> ShapeTypes = new Dictionary<ShapeType, Type>
        {
            { ShapeType.Circle, typeof(Circle) },
            { ShapeType.Triangle, typeof(Triangle) },
            { ShapeType.Square, typeof(Square) },
        };

        public Shape Create(ShapeType shapeType)
        {
            var type = ShapeTypes[shapeType];
            return (Shape)Activator.CreateInstance(type);
        }
    }
}
