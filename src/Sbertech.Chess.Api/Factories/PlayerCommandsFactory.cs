﻿using System.Collections.Generic;
using System.Linq;
using Sbertech.Chess.Api.Interfaces;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Api.Players;

namespace Sbertech.Chess.Api.Factories
{
    public class PlayerCommandsFactory : IPlayerCommandsFactory
    {
        public IReadOnlyList<PlayerCommand> CreateCommands(Board board)
        {
            var result = new List<PlayerCommand>();

            var shapes = board.Shapes
                .OrderBy(s => s.Position.X)
                .ThenBy(s => s.Position.Y)
                .ToList();

            var movings = shapes
                .Select(shape => new MovingCommand(board, shape))
                .ToList();

            var survives = shapes
                .Select(shape => new SurviveCommand(board, shape))
                .ToList();

            for (var i = 0; i < shapes.Count; i++)
            {
                result.Add(movings[i]);
                result.Add(survives[i]);
            }

            return result;
        }
    }
}
