﻿using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Interfaces
{
    public interface IPlayer
    {
        void Initialize(Board board);

        bool Play();
    }
}