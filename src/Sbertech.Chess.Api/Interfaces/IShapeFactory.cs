﻿using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Api.Interfaces
{
    public interface IShapeFactory
    {
        Shape Create(ShapeType shapeType);
    }
}