﻿using System.Collections.Generic;
using Sbertech.Chess.Api.Models;
using Sbertech.Chess.Api.Players;

namespace Sbertech.Chess.Api.Interfaces
{
    public interface IPlayerCommandsFactory
    {
        IReadOnlyList<PlayerCommand> CreateCommands(Board board);
    }
}
