﻿using System.Collections.Generic;

namespace Sbertech.Chess.Api.Models
{
    public class Circle : Shape
    {
        private static readonly MovingSequence[] AllowedMovings =
        {
            new MovingSequence(new[] { Moving.Right(), }),
            new MovingSequence(new[] { Moving.Down(), }),
        };

        public override ShapeType ShapeType => ShapeType.Circle;

        public override int NeighborsToSurvive => 1;

        public override IReadOnlyList<MovingSequence> GetAllowedMovings() => AllowedMovings;
    }
}