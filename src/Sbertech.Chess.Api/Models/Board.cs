﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sbertech.Chess.Api.Constants;
using Sbertech.Chess.Api.Exceptions;
using Sbertech.Chess.Api.Interfaces;

namespace Sbertech.Chess.Api.Models
{
    public class Board
    {
        private readonly IShapeFactory _shapeFactory;
        private Shape[,] _shapesMap;
        private List<Shape> _shapesList;

        public Board(IShapeFactory shapeFactory)
        {
            _shapeFactory = shapeFactory;
            SetSize(new Size(BoardContants.DefaultWidth, BoardContants.DefaultHeight));
        }

        public IReadOnlyList<Shape> Shapes => _shapesList;

        public int Width { get; private set; }

        public int Height { get; private set; }

        public Shape ActiveShape { get; set; }

        public void SetSize(Size size)
        {
            if (size.Width < 1 || size.Height < 1 || size.Width > BoardContants.MaxWidth || size.Height > BoardContants.MaxHeight)
            {
                throw new InvalidBoardException();
            }

            Width = size.Width;
            Height = size.Height;
            _shapesList = new List<Shape>();
            _shapesMap = new Shape[size.Width, size.Height];
        }

        public Shape Add(ShapeType shapeType, Position position)
        {
            if (position.X < 0 || position.X >= Width || position.Y < 0 || position.Y >= Height)
            {
                throw new InvalidShapeException($"The {shapeType} located {position} shape is out of board");
            }

            if (_shapesMap[position.X, position.Y] != null)
            {
                throw new InvalidShapeException($"The {shapeType} can't be placed into {position}, it's not empty");
            }

            var shape = _shapeFactory.Create(shapeType);
            shape.Position = position;
            _shapesList.Add(shape);
            _shapesMap[position.X, position.Y] = shape;

            return shape;
        }

        public bool TryAdd(ShapeType shapeType, Position position, out Shape shape)
        {
            try
            {
                shape = Add(shapeType, position);
                return true;
            }
            catch
            {
                shape = null;
                return false;
            }
        }

        public Shape Get(Position position)
        {
            return _shapesList.FirstOrDefault(s => s.Position == position);
        }

        public void Remove(Shape shape)
        {
            _shapesList.Remove(shape);
            _shapesMap[shape.Position.X, shape.Position.Y] = null;
        }

        public void Move(Shape shape, Moving moving)
        {
            var newPosition = shape.Position + moving;

            if (newPosition.X < 0 || newPosition.X >= Width || newPosition.Y < 0 || newPosition.Y >= Height)
            {
                throw new MoveOutOfBoardException(this, shape, moving);
            }

            _shapesMap[shape.Position.X, shape.Position.Y] = null;
            shape.Position = shape.Position + moving;
            _shapesMap[shape.Position.X, shape.Position.Y] = shape;
        }

        public IReadOnlyList<Shape> GetNeighbors(Position position)
        {
            return _shapesList.Where(IsNeighborsOf(position)).ToList();
        }

        private static Func<Shape, bool> IsNeighborsOf(Position position)
        {
            return shape => shape.Position.Is(position.X, position.Y - 1) ||
                shape.Position.Is(position.X, position.Y + 1) ||
                shape.Position.Is(position.X - 1, position.Y) ||
                shape.Position.Is(position.X + 1, position.Y);
        }
    }
}
