﻿namespace Sbertech.Chess.Api.Models
{
    public struct Position
    {
        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }

        public int Y { get; set; }

        public static Position operator +(Position position, Moving moving)
        {
            return new Position(position.X + moving.Dx, position.Y + moving.Dy);
        }

        public static Moving operator -(Position fromPosition, Position toPosition)
        {
            return new Moving(toPosition.X - fromPosition.X, toPosition.Y - fromPosition.Y);
        }

        public static bool operator ==(Position position1, Position position2) => position1.X == position2.X && position1.Y == position2.Y;

        public static bool operator !=(Position position1, Position position2) => !(position1 == position2);

        public bool Is(int x, int y)
        {
            return X == x && Y == y;
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }
    }
}
