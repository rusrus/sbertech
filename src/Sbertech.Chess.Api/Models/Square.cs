﻿using System.Collections.Generic;

namespace Sbertech.Chess.Api.Models
{
    public class Square : Shape
    {
        private static readonly MovingSequence[] AllowedMovings =
        {
            new MovingSequence(new[] { Moving.Right(), Moving.Right(), }),
            new MovingSequence(new[] { Moving.Right(), Moving.DownRight(), }),
            new MovingSequence(new[] { Moving.DownRight(), Moving.Right(), }),
            new MovingSequence(new[] { Moving.DownRight(), Moving.DownRight(), }),
            new MovingSequence(new[] { Moving.DownRight(), Moving.Down(), }),
            new MovingSequence(new[] { Moving.Down(), Moving.DownRight(), }),
            new MovingSequence(new[] { Moving.Down(), Moving.Down(), }),
            new MovingSequence(new[] { Moving.Down(), }),
            new MovingSequence(new[] { Moving.Right(), }),
            new MovingSequence(new[] { Moving.DownRight(), }),
        };

        public override ShapeType ShapeType => ShapeType.Square;

        public override int NeighborsToSurvive => 3;

        public override IReadOnlyList<MovingSequence> GetAllowedMovings() => AllowedMovings;
    }
}