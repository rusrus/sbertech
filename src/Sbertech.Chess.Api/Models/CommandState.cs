﻿namespace Sbertech.Chess.Api.Models
{
    public enum CommandState
    {
        NotStarted = 0,
        InProgress = 1,
        Complete = 2,
    }
}