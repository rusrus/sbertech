﻿namespace Sbertech.Chess.Api.Models
{
    public enum ShapeType
    {
        Circle = 1,
        Triangle = 2,
        Square = 3,
    }
}
