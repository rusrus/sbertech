﻿using System.Collections.Generic;

namespace Sbertech.Chess.Api.Models
{
    public abstract class Shape
    {
        public abstract ShapeType ShapeType { get; }

        public Position Position { get; internal set; }

        public abstract int NeighborsToSurvive { get; }

        public abstract IReadOnlyList<MovingSequence> GetAllowedMovings();
    }
}
