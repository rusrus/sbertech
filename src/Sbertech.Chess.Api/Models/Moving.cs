﻿namespace Sbertech.Chess.Api.Models
{
    public struct Moving
    {
        public Moving(int dx, int dy)
        {
            Dx = dx;
            Dy = dy;
        }

        public int Dx { get; }

        public int Dy { get; }

        public static Moving Right() => new Moving(+1, 0);

        public static Moving Down() => new Moving(0, +1);

        public static Moving DownRight() => new Moving(+1, +1);

        public override string ToString()
        {
            var dxsign = Dx > 0 ? "+" : string.Empty;
            var dysign = Dy > 0 ? "+" : string.Empty;
            return $"<{dxsign}{Dx}, {dysign}{Dy}>";
        }
    }
}