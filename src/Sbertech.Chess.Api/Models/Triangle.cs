﻿using System.Collections.Generic;

namespace Sbertech.Chess.Api.Models
{
    public class Triangle : Shape
    {
        private static readonly MovingSequence[] AllowMovings =
        {
            new MovingSequence(new[] { Moving.Right(), Moving.Right(), }),
            new MovingSequence(new[] { Moving.Down(), Moving.Down(), }),
            new MovingSequence(new[] { Moving.DownRight() }),
            new MovingSequence(new[] { Moving.Right() }),
            new MovingSequence(new[] { Moving.Down() }),
        };

        public override ShapeType ShapeType => ShapeType.Triangle;

        public override int NeighborsToSurvive => 2;

        public override IReadOnlyList<MovingSequence> GetAllowedMovings() => AllowMovings;
    }
}