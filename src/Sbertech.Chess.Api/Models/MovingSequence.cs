﻿using System.Collections.Generic;
using System.Linq;

namespace Sbertech.Chess.Api.Models
{
    public class MovingSequence
    {
        public MovingSequence(IReadOnlyList<Moving> steps)
        {
            Steps = steps;
            var dx = steps.Sum(m => m.Dx);
            var dy = steps.Sum(m => m.Dy);

            TargetMoving = new Moving(dx, dy);
            Distance = dx + dy;
        }

        public IReadOnlyList<Moving> Steps { get; }

        public Moving TargetMoving { get; }

        public int Distance { get; }
    }
}