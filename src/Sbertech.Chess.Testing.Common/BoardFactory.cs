﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sbertech.Chess.Api.Factories;
using Sbertech.Chess.Api.Models;

namespace Sbertech.Chess.Testing.Common
{
    public class BoardFactory
    {
        private static readonly Dictionary<char, ShapeType> ShapeTypes = new Dictionary<char, ShapeType>
        {
            { 'o', ShapeType.Circle },
            { '#', ShapeType.Square },
            { '^', ShapeType.Triangle },
        };

        public Board Create(string description)
        {
            var lines = description
                .ToLower()
                .Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .Select(line => line.Trim())
                .ToArray();

            var board = new Board(new ShapeFactory());
            board.SetSize(new Size(lines[0].Length, lines.Length));

            for (var y = 0; y < board.Height; y++)
            {
                var line = lines[y];
                for (var x = 0; x < board.Width && x < line.Length; x++)
                {
                    if (!ShapeTypes.TryGetValue(line[x], out var type))
                    {
                        continue;
                    }

                    board.Add(type, new Position(x, y));
                }
            }

            return board;
        }
    }
}
